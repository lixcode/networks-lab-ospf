#include "router.h"


RouterLink::RouterLink (Router *router, RouterLink *prev)
	: router (router)
	, prev (prev) {

}

bool RouterLink::operator == (const RouterLink &rl) const {
	return this->router == rl.router;
}

bool Router::isNull () {
	return
		neightbors [0] == nullptr &&
		neightbors [1] == nullptr &&
		neightbors [2] == nullptr &&
		neightbors [3] == nullptr;
}

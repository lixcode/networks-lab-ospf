#ifndef ROUTER_H
#define ROUTER_H

#include <QHostAddress>



struct Router {
	QHostAddress network;
	QHostAddress virtualIP;
	QHostAddress localIP;
	int port;

	QList <Router *> neightbors = { nullptr, nullptr, nullptr, nullptr };

	bool pathNeedRecalc = true;
	QStringList path;

	Router () = default;

	bool isNull ();
};

struct RouterLink {
	Router *router;
	RouterLink *prev;

	RouterLink (Router *router, RouterLink *prev);

	bool operator == (const RouterLink &rl) const;
};

#endif // ROUTER_H

#include "server.h"

#include <QAbstractSocket>
#include <QDateTime>
#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QSettings>
#include <qhttpclientrequest.hpp>
#include <qhttpclientresponse.hpp>


Server::Server (QObject *parent) : QObject (parent) {
	n1.setActivePort (false);
	n2.setActivePort (false);
	n3.setActivePort (false);
	n4.setActivePort (false);
	m_table.append (neighbor1 () );
	m_table.append (neighbor2 () );
	m_table.append (neighbor3 () );
	m_table.append (neighbor4 () );

	server = new qhttp::server::QHttpServer ();

	map [m_ip] = &router;

	server->listen (
		QHostAddress::Any,
		0,
		[this] (qhttp::server::QHttpRequest *req, qhttp::server::QHttpResponse *res) {
		req->collectData ();

		req->onEnd ( [this, req, res] () {
			const QByteArray &ba	= req->collectedData ();
			QJsonDocument doc		= QJsonDocument::fromJson (ba);
			QJsonObject obj			= doc.object ();
			QString type			= obj ["type"].toString ();

			qDebug () << "server content" << ba.size ();
			qDebug ("%s", QString::fromUtf8 (ba).toLocal8Bit ().data () );

			if (type == "ping") {
				this->processPing (res, obj);
			}
			if (type == "sync") {
				QString state = obj ["state"].toString ();

				if (state == "up") {
					this->processConnect (res, obj);
				}
				else if (state == "down") {
					this->processDisconnect (res, obj);
				}
				else {
					res->addHeader (QString ("Content-Length").toUtf8 (), "0");
					res->addHeader (QString ("content-type").toLatin1 (), QString ("text/json; charset=UTF-8").toLatin1 () );
					res->setStatusCode (qhttp::ESTATUS_BAD_REQUEST);
					res->end ();
					qDebug () << "server unknown state" << state;
				}
			}
			else if (type == "ospf") {
				while (isProcessingOSPF) {
					;
				}
				isProcessingOSPF = true;

				QString command = obj ["command"].toString ();

				if (command == "discover") {
					this->processOspfDiscover (res, obj);
				}
				else if (command == "add") {
					this->processOspfAdd (res, obj);
				}
				else if (command == "remove") {
					this->processOspfRemove (res, obj);
				}
				else if (command == "bd-add") {
					this->processOspfBdAdd (res, obj);
				}
				else if (command == "bd-remove") {
					this->processOspfBdRemove (res, obj);
				}
				else {
					res->addHeader (QString ("Content-Length").toUtf8 (), "0");
					res->addHeader (QString ("content-type").toLatin1 (), QString ("text/json; charset=UTF-8").toLatin1 () );
					res->setStatusCode (qhttp::ESTATUS_BAD_REQUEST);
					res->end ();
					qDebug () << "server unknown command" << command;
				}
				isProcessingOSPF = false;
			}
			else {
				QByteArray bao = QString (R"({"type":"error","message":"Bad request!"})").toUtf8 ();

				res->addHeader (QString ("Content-Length").toUtf8 (), QString::number (bao.size () ).toLatin1 () );
				res->addHeader (QString ("content-type").toLatin1 (), QString ("text/json; charset=UTF-8").toLatin1 () );
				res->setStatusCode (qhttp::ESTATUS_BAD_REQUEST);
				res->end (bao);
				qDebug () << "server unknown type" << type;
			}
		});
	});

	if (server->isListening () ) {
		setPort (server->tcpServer ()->serverPort () );
		setAddress (server->tcpServer ()->serverAddress ().toString () + ":" + QString::number (m_port) );
		setDrPort (m_port);
	}
	else {
		qDebug () << server->tcpServer ()->errorString ();
	}

	router.localIP			= QHostAddress ("127.0.0.1");
	router.virtualIP		= m_ip;
	router.port				= m_port;
	router.pathNeedRecalc	= false;
}

Server::~Server () {
	for (Neighbor *n : m_table) {
		delete n;
	}

	if (server != nullptr) {
		server->stopListening ();
	}
}

QString Server::address () const {
	return m_address;
}

QHostAddress Server::ip () const {
	return m_ip;
}

QHostAddress Server::mask () const {
	return m_mask;
}

QHostAddress Server::gateway () const {
	return m_gateway;
}

QList <Neighbor *> Server::table () const {
	return m_table;
}

QString Server::name () const {
	return m_name;
}

int Server::port () const {
	return m_port;
}

QString Server::toJSON () {
	QJsonDocument	doc;
	QJsonObject		obj;
	QJsonArray		arr;
	QString			str;

	for (int i = 0; i < 4; i++) {
		arr.append (m_table [i]->toJSON () );
	}

	obj.insert ("ip", m_ip.toString () );
	obj.insert ("mask", m_mask.toString () );
	obj.insert ("gateway", m_gateway.toString () );
	obj.insert ("port", m_port);
	obj.insert ("table", arr);

	doc.setObject (obj);
	str = QString::fromUtf8 (doc.toJson () );

	qDebug () << str;
	return str;
}

bool Server::fromJSON (const QString &json) {
	QJsonParseError err;
	QJsonDocument	doc = QJsonDocument::fromJson (json.toUtf8 (), &err);
	QJsonObject		obj = doc.object ();
	QJsonArray		arr = obj ["table"].toArray ();

	setIp (QHostAddress (obj ["ip"].toString () ) );
	setMask (QHostAddress (obj ["mask"].toString () ) );
	setGateway (QHostAddress (obj ["gateway"].toString () ) );
	setPort (obj ["port"].toInt () );

	for (int i = 0; i < 4; i++) {
		m_table [i]->fromJSON (arr [i].toObject () );
	}

	return err.error == QJsonParseError::NoError;
}

void Server::connTo (Neighbor *neightbor) {
	QJsonObject obj;
	auto		*client = new qhttp::client::QHttpClient ();

	connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

	obj.insert ("type", "sync");
	obj.insert ("state", "up");
	obj.insert ("from", m_ip.toString () );
	obj.insert ("to", neightbor->address ().toString () );

	neightbor->setActivePort (false);

	client->request (
		qhttp::EHTTP_GET,
		"http://" + neightbor->ip ().toString () + ":" + QString::number (neightbor->ipPort () ),
		[obj] (qhttp::client::QHttpRequest *req) {
		QJsonDocument doc;
		QByteArray ba;

		doc.setObject (obj);
		ba = doc.toJson ();

		req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
		req->end (ba);
	},
		[this, neightbor] (qhttp::client::QHttpResponse *res) {
		res->collectData ();
		res->onEnd ( [res, this, neightbor] () {
			QJsonDocument   doc = QJsonDocument::fromJson (res->collectedData () );
			QJsonObject     obj = doc.object ();

			if (obj ["type"].toString () == "sync-response") {
				if (obj ["success"].toString () == "yes") {
					neightbor->setActivePort (true);
					this->finishLink (neightbor);
				}
			}
			else {
				qDebug () << "wrong type" << obj ["type"].toString ();
			}
		});
	});
}

void Server::disFrom (Neighbor *neightbor) {
	QJsonObject obj;
	auto		*client = new qhttp::client::QHttpClient ();

	connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

	obj.insert ("type", "sync");
	obj.insert ("state", "down");
	obj.insert ("from", m_ip.toString () );
	obj.insert ("to", neightbor->address ().toString () );

	neightbor->setActivePort (true);

	client->request (
		qhttp::EHTTP_GET,
		"http://" + neightbor->ip ().toString () + ":" + QString::number (neightbor->ipPort () ),
		[obj] (qhttp::client::QHttpRequest *req) {
		QJsonDocument doc;
		QByteArray ba;

		doc.setObject (obj);
		ba = doc.toJson ();

		req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
		req->end (ba);
	},
		[this, neightbor] (qhttp::client::QHttpResponse *res) {
		res->collectData ();
		res->onEnd ( [res, this, neightbor] () {
			QJsonDocument   doc = QJsonDocument::fromJson (res->collectedData () );
			QJsonObject     obj = doc.object ();

			if (obj ["type"].toString () == "sync-response") {
				if (obj ["success"].toString () == "yes") {
					neightbor->setActivePort (false);
					this->finishUnlink (neightbor);
				}
			}
			else {
				qDebug () << "wrong type" << obj ["type"].toString ();
			}
		});
	});
}

QString Server::ipT () const {
	return m_ip.toString ();
}

QString Server::maskT () const {
	return m_mask.toString ();
}

QString Server::gatewayT () const {
	return m_gateway.toString ();
}

Neighbor * Server::neighbor1 () {
	return &n1;
}

Neighbor * Server::neighbor2 () {
	return &n2;
}

Neighbor * Server::neighbor3 () {
	return &n3;
}

Neighbor * Server::neighbor4 () {
	return &n4;
}

QString Server::drotherIP () const {
	return m_drotherIP.toString ();
}

int Server::drotherPort () const {
	return m_drotherPort;
}

QString Server::externIP () const {
	return m_externIP.toString ();
}

QString Server::drIP () const {
	return m_drIP.toString ();
}

int Server::drPort () const {
	return m_drPort;
}

void Server::setAddress (const QString &address) {
	if (m_address == address) {
		return;
	}

	m_address = address;
	emit addressChanged (m_address);
}

void Server::setIp (const QHostAddress &ip) {
	if (m_ip == ip) {
		return;
	}

	map.remove (m_ip);
	map [ip] = &router;

	m_ip				= ip;
	router.virtualIP	= m_ip;
	setIpT (m_ip.toString () );
	emit ipChanged (m_ip);
}

void Server::setMask (const QHostAddress &mask) {
	if (m_mask == mask) {
		return;
	}

	m_mask = mask;
	setMaskT (m_mask.toString () );
	emit maskChanged (m_mask);
}

void Server::setGateway (const QHostAddress &gateway) {
	if (m_gateway == gateway) {
		return;
	}

	m_gateway = gateway;
	setGatewayT (m_gateway.toString () );
	emit gatewayChanged (m_gateway);
}

void Server::setTable (const QList <Neighbor *> &table) {
	if (m_table == table) {
		return;
	}

	m_table = table;
	emit tableChanged (m_table);
}

void Server::setName (const QString &name) {
	if (m_name == name) {
		return;
	}

	m_name = name;
	emit nameChanged (m_name);

	if (m_table.isEmpty () ) {
		QString json = QSettings ().value ("server_" + name, toJSON () ).toString ();

		fromJSON (json);
	}
}

void Server::setPort (int port) {
	if (m_port == port) {
		return;
	}

	m_port = port;
	emit portChanged (m_port);
}

void Server::setIpT (QString ipT) {
	if (m_ip.toString () == ipT) {
		return;
	}

	m_ipT = ipT;
	setIp (QHostAddress (m_ipT) );
	emit ipTChanged (m_ipT);
}

void Server::setMaskT (QString maskT) {
	if (m_mask.toString () == maskT) {
		return;
	}

	m_maskT = maskT;
	setMask (QHostAddress (m_maskT) );
	emit maskTChanged (m_maskT);
}

void Server::setGatewayT (QString gatewayT) {
	if (m_gateway.toString () == gatewayT) {
		return;
	}

	m_gatewayT = gatewayT;
	setGateway (QHostAddress (m_gatewayT) );
	emit gatewayTChanged (m_gatewayT);
}

void Server::setNeighbor1 (Neighbor *neighbor1) {
	if (m_neighbor1 == neighbor1) {
		return;
	}

	m_neighbor1 = neighbor1;
	emit neighbor1Changed (m_neighbor1);
}

void Server::setNeighbor2 (Neighbor *neighbor2) {
	if (m_neighbor2 == neighbor2) {
		return;
	}

	m_neighbor2 = neighbor2;
	emit neighbor2Changed (m_neighbor2);
}

void Server::setNeighbor3 (Neighbor *neighbor3) {
	if (m_neighbor3 == neighbor3) {
		return;
	}

	m_neighbor3 = neighbor3;
	emit neighbor3Changed (m_neighbor3);
}

void Server::setNeighbor4 (Neighbor *neighbor4) {
	if (m_neighbor4 == neighbor4) {
		return;
	}

	m_neighbor4 = neighbor4;
	emit neighbor4Changed (m_neighbor4);
}

void Server::setDrotherIP (QString drotherIP) {
	if (m_drotherIP.toString () == drotherIP) {
		return;
	}

	m_drotherIP = QHostAddress (drotherIP);
	mapCheckDR ();
	emit drotherIPChanged (drotherIP);
}

void Server::setDrotherPort (int drotherPort) {
	if (m_drotherPort == drotherPort) {
		return;
	}

	m_drotherPort = drotherPort;
	emit drotherPortChanged (m_drotherPort);
}

void Server::setExternIP (QString externIP) {
	if (m_externIP.toString () == externIP) {
		return;
	}

	m_externIP		= QHostAddress (externIP);
	router.localIP	= m_externIP;
	emit externIPChanged (externIP);
}

void Server::setDrIP (QString drIP) {
	if (m_drIP.toString () == drIP) {
		return;
	}

	m_drIP = QHostAddress (drIP);
	emit drIPChanged (drIP);
}

void Server::setDrPort (int drPort) {
	if (m_drPort == drPort) {
		return;
	}

	m_drPort = drPort;
	emit drPortChanged (m_drPort);
}

void Server::finishLink (Neighbor *neightbor) {
	QJsonObject obj;
	auto		*client = new qhttp::client::QHttpClient ();

	connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

	obj.insert ("type", "ospf");
	obj.insert ("command", "bd-add");
	obj.insert ("from", m_ip.toString () );
	obj.insert ("to", m_drotherIP.toString () );
	obj.insert ("r1", m_ip.toString () );
	obj.insert ("ip1", m_externIP.toString () );
	obj.insert ("p1", m_port);
	obj.insert ("r2", neightbor->address ().toString () );
	obj.insert ("ip2", neightbor->ip ().toString () );
	obj.insert ("p2", neightbor->ipPort () );

	client->request (
		qhttp::EHTTP_GET,
		"http://" + m_drIP.toString () + ":" + QString::number (m_drPort),
		[obj] (qhttp::client::QHttpRequest *req) {
		QJsonDocument doc;
		QByteArray ba;

		doc.setObject (obj);
		ba = doc.toJson ();

		req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
		req->end (ba);
	},
		[] (qhttp::client::QHttpResponse *) {
		// Do nothing
	});
}

void Server::finishUnlink (Neighbor *neightbor) {
	QJsonObject obj;
	auto		*client = new qhttp::client::QHttpClient ();

	connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

	obj.insert ("type", "ospf");
	obj.insert ("command", "bd-remove");
	obj.insert ("from", m_ip.toString () );
	obj.insert ("to", m_drotherIP.toString () );
	obj.insert ("r1", m_ip.toString () );
	obj.insert ("r2", neightbor->address ().toString () );

	neightbor->setActivePort (false);

	client->request (
		qhttp::EHTTP_GET,
		"http://" + m_drIP.toString () + ":" + QString::number (m_drPort),
		[obj] (qhttp::client::QHttpRequest *req) {
		QJsonDocument doc;
		QByteArray ba;

		doc.setObject (obj);
		ba = doc.toJson ();

		req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
		req->end (ba);
	},
		[] (qhttp::client::QHttpResponse *) {
		// Do nothing
	});
}

void Server::processPing (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	QJsonDocument	doc;
	QJsonObject		obj1;

	obj1.insert (QStringLiteral ("type"), "ping-response");
	obj1.insert (QStringLiteral ("from"), m_ip.toString () );
	obj1.insert (QStringLiteral ("to"), obj ["from"]);
	obj1.insert (QStringLiteral ("time"), obj ["time"]);

	doc.setObject (obj1);
	res->setStatusCode (qhttp::ESTATUS_OK);

	QByteArray ba = doc.toJson ();

	res->addHeader (QString ("Content-Length").toLatin1 (), QString::number (ba.size () ).toLatin1 () );
	res->end (ba);
}

void Server::processConnect (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	QJsonDocument	doc;
	QJsonObject		obj1;

	obj1.insert (QStringLiteral ("type"), "sync-response");
	obj1.insert (QStringLiteral ("from"), obj ["to"]);
	obj1.insert (QStringLiteral ("to"), obj ["from"]);

	QString from	= obj ["from"].toString ();
	bool	found	= false;

	if (from == m_ip.toString () ) {
		found = true;
	}

	for (auto *neightbor : m_table) {
		if (neightbor->addressT () == from) {
			neightbor->setActivePort (true);
			finishLink (neightbor);
			found = true;
		}
	}

	obj1 ["success"] = found ? "yes" : "no";

	doc.setObject (obj1);
	res->setStatusCode (qhttp::ESTATUS_OK);

	QByteArray ba = doc.toJson ();

	res->addHeader (QString ("Content-Length").toLatin1 (), QString::number (ba.size () ).toLatin1 () );
	res->end (ba);
}

void Server::processDisconnect (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	QJsonDocument	doc;
	QJsonObject		obj1;

	obj1.insert (QStringLiteral ("type"), "sync-response");
	obj1.insert (QStringLiteral ("from"), obj ["to"]);
	obj1.insert (QStringLiteral ("to"), obj ["from"]);

	QString from	= obj ["from"].toString ();
	bool	found	= false;

	if (from == m_ip.toString () ) {
		found = true;
	}

	for (auto *neightbor : m_table) {
		if (neightbor->addressT () == from) {
			neightbor->setActivePort (false);
			finishUnlink (neightbor);
			found = true;
		}
	}

	obj1 ["success"] = found ? "yes" : "no";

	doc.setObject (obj1);
	res->setStatusCode (qhttp::ESTATUS_OK);

	QByteArray ba = doc.toJson ();

	res->addHeader (QString ("Content-Length").toLatin1 (), QString::number (ba.size () ).toLatin1 () );
	res->end (ba);
}

void Server::processOspfDiscover (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	QJsonDocument	doc;
	QJsonObject		obj1;
	QJsonArray		arr;

	for (Neighbor *neightbor : m_table) {
		if (neightbor->activePort () ) {
			arr.push_back (neightbor->toJSON () );
		}
	}

	obj1.insert (QStringLiteral ("type"), "discover-response");
	obj1.insert (QStringLiteral ("from"), m_ip.toString () );
	obj1.insert (QStringLiteral ("to"), obj ["from"]);
	obj1.insert (QStringLiteral ("table"), arr);

	doc.setObject (obj1);
	res->setStatusCode (qhttp::ESTATUS_OK);

	QByteArray ba = doc.toJson ();

	res->addHeader (QString ("Content-Length").toLatin1 (), QString::number (ba.size () ).toLatin1 () );
	res->end (ba);
}

void Server::processOspfAdd (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	auto	a1	= QHostAddress (obj ["r1"].toString () );
	auto	a2	= QHostAddress (obj ["r2"].toString () );

	mapAddLink (a1, a2, obj);

	res->end ();
}

void Server::processOspfRemove (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	auto	a1	= QHostAddress (obj ["r1"].toString () );
	auto	a2	= QHostAddress (obj ["r2"].toString () );

	mapRemoveLink (a1, a2);

	res->end ();
}

void Server::processOspfBdAdd (qhttp::server::QHttpResponse *res, QJsonObject &obj) {

	auto	obj0	= QJsonObject ();
	auto	a1		= QHostAddress (obj ["r1"].toString () );
	auto	a2		= QHostAddress (obj ["r2"].toString () );

	obj0.insert ("type", "ospf");
	obj0.insert ("command", "add");
	obj0.insert ("from", m_ip.toString () );
	obj0.insert ("r1", obj ["r1"]);
	obj0.insert ("ip1", obj ["ip1"]);
	obj0.insert ("p1", obj ["p1"]);
	obj0.insert ("r2", obj ["r2"]);
	obj0.insert ("ip2", obj ["ip2"]);
	obj0.insert ("p2", obj ["p2"]);

	for (auto *r : map) {
		auto obj1 = QJsonObject (obj0);
		obj1.insert ("to", r->virtualIP.toString () );

		auto *client = new qhttp::client::QHttpClient ();
		connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

		client->request (
			qhttp::EHTTP_GET,
			"http://" + r->localIP.toString () + ":" + QString::number (r->port),
			[obj1] (qhttp::client::QHttpRequest *req) {
			QJsonDocument doc;
			QByteArray ba;

			doc.setObject (obj1);
			ba = doc.toJson ();

			req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
			req->end (ba);
		},
			[] (qhttp::client::QHttpResponse *) {
			// Do nothing
		});
	}

	res->end ();
}

void Server::processOspfBdRemove (qhttp::server::QHttpResponse *res, QJsonObject &obj) {

	auto	obj0	= QJsonObject ();
	auto	a1		= QHostAddress (obj ["r1"].toString () );
	auto	a2		= QHostAddress (obj ["r2"].toString () );

	obj0.insert ("type", "ospf");
	obj0.insert ("command", "remove");
	obj0.insert ("from", m_ip.toString () );
	obj0.insert ("r1", obj ["r1"]);
	obj0.insert ("r2", obj ["r2"]);

	for (auto *r : map) {
		auto obj1 = QJsonObject (obj0);
		obj1.insert ("to", r->virtualIP.toString () );

		auto *client = new qhttp::client::QHttpClient ();
		connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

		client->request (
			qhttp::EHTTP_GET,
			"http://" + r->localIP.toString () + ":" + QString::number (r->port),
			[obj1] (qhttp::client::QHttpRequest *req) {
			QJsonDocument doc;
			QByteArray ba;

			doc.setObject (obj1);
			ba = doc.toJson ();

			req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
			req->end (ba);
		},
			[] (qhttp::client::QHttpResponse *) {
			// Do nothing
		});
	}

	res->end ();
}

bool Server::mapHasLink (Router *r1, Router *r2) {

	if (r1 == nullptr || r2 == nullptr) {
		return false;
	}

	return r1->neightbors.contains (r2);
}

bool Server::mapAddLink (QHostAddress &a1, QHostAddress &a2, QJsonObject &obj) {
	auto	*r1 = map.value (a1, nullptr);
	auto	*r2 = map.value (a2, nullptr);

	if (mapHasLink (r1, r2) ) {
		return false;
	}

	if (r1 == nullptr && r2 == nullptr) {
		qDebug () << "link failed unknown devices" << a1 << a2;
		return false;
	}

	emit newConn (a1.toString (), a2.toString () );

	if (r1 == nullptr) {
		r1				= map.insert (a1, new Router).value ();
		r1->virtualIP	= QHostAddress (a1);
		r1->localIP		= QHostAddress (obj ["ip1"].toString () );
		r1->port		= obj ["p1"].toInt ();

		mapDiscover (r1);
	}

	if (r2 == nullptr) {
		r2				= map.insert (a2, new Router).value ();
		r2->virtualIP	= QHostAddress (a2);
		r2->localIP		= QHostAddress (obj ["ip2"].toString () );
		r2->port		= obj ["p2"].toInt ();

		mapDiscover (r2);
	}

	mapMarkNeedUpdate (a1.toString (), a2.toString () );

	r1->neightbors.replace (r1->neightbors.indexOf (nullptr), r2);
	r2->neightbors.replace (r2->neightbors.indexOf (nullptr), r1);

	mapReleaseUpdate ();
	mapCheckDR ();

	return true;
}

bool Server::mapRemoveLink (QHostAddress &a1, QHostAddress &a2) {
	auto	*r1 = map.value (a1, nullptr);
	auto	*r2 = map.value (a2, nullptr);

	if (!mapHasLink (r1, r2) ) {
		return false;
	}

	if (r1 == nullptr || r2 == nullptr) {
		qDebug () << "one or both devices are unknown" << a1 << a2;
		return false;
	}

	mapMarkNeedUpdate (a1.toString (), a2.toString () );

	r1->neightbors.replace (r1->neightbors.indexOf (r2), nullptr);
	r2->neightbors.replace (r2->neightbors.indexOf (r1), nullptr);
	emit connLost (a1.toString (), a2.toString () );

	if (r1->isNull () && r1 != &router) {
		emit routerLost (r1->virtualIP.toString () );
		map.remove (a1);
		delete r1;
		mapCheckDR ();
	}

	if (r2->isNull () && r2 != &router) {
		emit routerLost (r2->virtualIP.toString () );
		map.remove (a2);
		delete r2;
		mapCheckDR ();
	}

	mapReleaseUpdate ();

	return true;
}

bool Server::mapUpdatePath (Router *r) {
	QList <RouterLink>	links = { RouterLink (&router, nullptr) };

	volatile int		count = 1;

	for (int i = 0; i < count; i++) {
		auto link = links [i];

		if (link.router->neightbors.contains (r) ) {
			QStringList newPath = { r->virtualIP.toString () };
			auto		*linkIt = &link;

			while (linkIt != nullptr) {
				newPath.prepend (linkIt->router->virtualIP.toString () );
				linkIt = linkIt->prev;
			}

			if (r->path != newPath) {
				if (r->path.isEmpty () ) {
					emit newRouter (
						r->virtualIP.toString (),
						newPath.join (" -> ") );
				}
				else {
					emit pathChanged (
						router.virtualIP.toString (),
						r->virtualIP.toString (),
						r->path.join (" -> "),
						newPath.join (" -> ") );
				}
				r->path = newPath;
			}

			r->pathNeedRecalc = false;

			return true;
		}
		else {
			for (auto *neightbor : link.router->neightbors) {
				auto new_link = RouterLink (neightbor, &links[i]);
				if (neightbor != nullptr && !links.contains(new_link)) {
					links.append (new_link);
					count++;
				}
			}
		}
	}

	// No path found, delete router from topology

	qDebug () << "no path found for" << r->virtualIP;
	return false;
}

void Server::mapMarkNeedUpdate (QString a1, QString a2) {
	for (auto r : map) {
		r->pathNeedRecalc |= r->path.contains (a1) || r->path.contains (a2);
	}
}

void Server::mapReleaseUpdate () {
	QList<Router*> rl;

	for (auto r : map) {
		if (r->pathNeedRecalc) {
			if (!mapUpdatePath (r)) {
				rl.append(r);
			}
		}
	}

	for (auto r : rl) {
		map.remove(r->virtualIP);
		delete r;
	}
}

void Server::mapDiscover (Router *r) {
	QJsonObject obj;
	auto		*client = new qhttp::client::QHttpClient ();

	connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

	obj.insert ("type", "ospf");
	obj.insert ("command", "discover");
	obj.insert ("from", m_ip.toString () );
	obj.insert ("to", r->virtualIP.toString () );

	client->request (
		qhttp::EHTTP_GET,
		"http://" + r->localIP.toString () + ":" + QString::number (r->port),
		[obj] (qhttp::client::QHttpRequest *req) {
		QJsonDocument doc;
		QByteArray ba;

		doc.setObject (obj);
		ba = doc.toJson ();

		req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
		req->end (ba);
	},
		[this, r] (qhttp::client::QHttpResponse *res) {
		res->collectData ();
		res->onEnd ( [res, this, r] () {
			QJsonDocument   doc = QJsonDocument::fromJson (res->collectedData () );
			QJsonObject     obj = doc.object ();

			if (obj ["type"].toString () == "discover-response") {
				auto table = obj ["table"].toArray ();

				for (auto row : table) {
					auto neightbor	= row.toObject ();
					auto host		= QHostAddress (neightbor ["address"].toString () );

					obj ["ip2"] = neightbor ["ip"];
					obj ["p2"]	= neightbor ["ipport"];
					this->mapAddLink (r->virtualIP, host, obj);
				}
			}
			else {
				qDebug () << "wrong type" << obj ["type"].toString ();
			}
		});
	});
}

void Server::mapCheckDR () {
	if (map.contains (m_drotherIP) ) {
		setDrIP (map [m_drotherIP]->localIP.toString () );
		setDrPort (map [m_drotherIP]->port);
	}
	else {
		setDrIP ("127.0.0.1");
		setDrPort (m_port);
	}
}

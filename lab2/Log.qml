import QtQuick 2.0
import QtQuick.Controls 2.1

import "ospf" as OSPF

Flickable {
	contentWidth: columb.width
	contentHeight: columb.height
	interactive: true
	boundsBehavior: Flickable.StopAtBounds

	function newConn (r1, r2) {
		new_conn.createObject(columb, {r1:r1, r2:r2})
	}

	function connLost (r1, r2) {
		conn_lost.createObject(columb, {r1:r1, r2:r2})
	}

	function newRouter (r, p) {
		new_router.createObject(columb, {r:r, p:p})
	}

	function routerLost (r) {
		router_lost.createObject(columb, {r:r})
	}

	function pathChanged (r1, r2, p1, p2) {
		path_changed.createObject(columb, {r1:r1, r2:r2, p1:p1, p2:p2})
	}

	Component {
		id: new_conn;
		OSPF.NewConn {}
	}

	Component {
		id: conn_lost;
		OSPF.ConnLost {}
	}

	Component {
		id: new_router;
		OSPF.NewRouter {}
	}

	Component {
		id: router_lost;
		OSPF.RouterLost {}
	}

	Component {
		id: path_changed;
		OSPF.PathChanged {}
	}

	Column {
		id: columb;

		OSPF.Header {
			text: "Logs"
			textColor: "#6204dd"
		}

		OSPF.Delimiter2 {}
	}
}

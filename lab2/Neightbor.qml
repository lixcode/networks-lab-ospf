import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

import Cpp 1.0

Column {
	property CppNeighbor neightbor: null
	id: content
	width: win.width

	SwitchDelegate {
		width: parent.width
		text: "Neightbor";

		position: neightbor.activePort ? 1.0 : 0.0;

		MouseArea {
			anchors.fill: parent;
			onClicked: {
				if (neightbor.activePort) {
					server.disFrom(neightbor);
				}
				else {
					server.connTo(neightbor);
				}
			}
		}
	}

	Component.onCompleted:  {
		console.log(server.table.length)
	}

	ItemDelegate {
		width: parent.width
		text: "IP/Mask";

		Row {
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter

			TextField {
				onTextChanged: neightbor.addressT = text
				Component.onCompleted: text = neightbor.addressT
			}

			TextField {
				onTextChanged: neightbor.maskT = text
				Component.onCompleted: text = neightbor.maskT
			}
		}
	}

	SwitchDelegate {
		width: parent.width
		text: "Ping time - " + neightbor.pingTime;

		property bool pinging: position == 1;

		onPingingChanged: {
			if (pinging) timer.start();
			else timer.stop();
		}
	}

	ItemDelegate {
		width: parent.width
		text: "IP/Port";


		Row {
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter

			TextField {
				onTextChanged: neightbor.ipT = text
				Component.onCompleted: text = neightbor.ipT
			}

			TextField {
				onTextChanged: neightbor.ipPort = Number(text)
				Component.onCompleted: text = neightbor.ipPort
				width: 60;
			}
		}
	}

	ItemDelegate {
	}

	Timer {
		id: timer;
		interval: 1500;
		repeat: true;
		running: false

		onTriggered: {
			neightbor.ping(server.ipT);
		}
	}

}

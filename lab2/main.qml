import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

import Cpp 1.0

ApplicationWindow {
	id: win
	visible: true
	width: 480
	height: 640
	title: qsTr("OSPF router")

	Material.theme: Material.Light
	Material.accent: Material.Blue
	Material.primary: Material.Teal

	Component {
		id: server_comp

		Server {
			clip: true
		}
	}

	SwipeView {
		id: swipe_view
		anchors {
			top: toolbar.bottom
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}
	}

	PageIndicator {
		id: indicator

		count: swipe_view.count
		currentIndex: swipe_view.currentIndex

		anchors.bottom: swipe_view.bottom
		anchors.horizontalCenter: parent.horizontalCenter
	}

	ToolBar {
		id: toolbar
		width: parent.width

		RowLayout {
			anchors.fill: parent

			Label {
				id: title
				text: "OSPF router"
			}

			Item {
				Layout.fillWidth: true
			}

			ToolSeparator {
			}

			ToolButton {
				property int counter: 0

				text: "Add"

				onClicked: {
					server_comp.createObject(swipe_view, {})
				}
			}
		}
	}
}

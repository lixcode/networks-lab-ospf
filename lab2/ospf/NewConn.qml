import QtQuick 2.0

Column {
	width: win.width

	property string r1: "127.0.0.1";
	property string r2: "127.0.0.2";

	Header {
		text: "->> New link between " + r1 + " and " + r2;
		textColor: "blue";
	}

	Delimiter2 {}
}

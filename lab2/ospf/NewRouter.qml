import QtQuick 2.0
import QtQuick.Controls 2.1


Column {
	width: win.width

	property string r: "127.0.0.2";
	property string p: "127.0.0.1 -> 127.0.0.3 -> 127.0.0.2"

	Header {
		text: "+ New router found: " + r;
		textColor: "green";
	}

	Comment {
		text: p
	}

	Delimiter {}
}

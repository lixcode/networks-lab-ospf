import QtQuick 2.0
import QtQuick.Controls 2.1

Column {
	width: win.width

	property string r1: "127.0.0.1";
	property string r2: "127.0.0.2";
	property string p1: "127.0.0.1 -> 127.0.0.3 -> 127.0.0.2";
	property string p2: "127.0.0.1 -> 127.0.0.2";

	Header {
		text: "~ Path from " + r1 + " to " + r2 + " changed";
		textColor: "#049657";
	}

	Comment {
		text: p1;
		font.strikeout: true;
		color: "grey";
	}

	Comment {
		text: p2;
	}

	Delimiter {}
}

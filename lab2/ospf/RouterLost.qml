import QtQuick 2.0
import QtQuick.Controls 2.1


Column {
	width: win.width

	property string r: "127.0.0.2";

	Header {

		text: "- Path to the router " + r + " lost.";
		textColor: "red";
	}

	Delimiter2 {}
}

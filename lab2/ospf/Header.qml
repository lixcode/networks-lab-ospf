import QtQuick 2.0
import QtQuick.Controls 2.1

ItemDelegate {
	id: control

	property color textColor: "black";
	font.family: "monospace"
	anchors.right: parent.right;
	anchors.left: parent.left;

	contentItem: Text {
		text: control.text
		font: control.font
		color: control.textColor
		elide: Text.ElideLeft
		verticalAlignment: Text.AlignVCenter
	}
}

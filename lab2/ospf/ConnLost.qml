import QtQuick 2.0

Column {
	width: win.width

	property string r1: "127.0.0.1";
	property string r2: "127.0.0.2";

	Header {
		text: "->< Connection between " + r1 + " and " + r2 + " is lost";
		textColor: "#0080ff";
	}

	Delimiter2 {}
}

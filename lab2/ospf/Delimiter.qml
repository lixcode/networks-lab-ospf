import QtQuick 2.0

Item {

	anchors.left: parent.left;
	anchors.right: parent.right;

	height: 10;

	Rectangle {
		anchors {
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}

		color: "#cacaca"
		height: 3
	}

}

import QtQuick 2.0
import QtQuick.Controls 2.1

Label {
	anchors.left: parent.left;
	anchors.right: parent.right;
	anchors.leftMargin: 25;
	anchors.rightMargin: 25;
	font.family: "monospace"
	wrapMode: Label.WrapAtWordBoundaryOrAnywhere
}

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.1

import Cpp 1.0

SwipeView {
	orientation: Qt.Vertical

	property CppServer server: CppServer {
		onNewConn: {
			log.newConn(r1, r2)
		}

		onConnLost: {
			log.connLost(r1, r2)
		}

		onNewRouter: {
			log.newRouter(r, p);
		}

		onRouterLost: {
			log.routerLost(r);
		}

		onPathChanged: {
			log.pathChanged(r1, r2, p1, p2)
		}
	}

	Flickable {
		contentWidth: columb.width
		contentHeight: columb.height
		interactive: true
		boundsBehavior: Flickable.StopAtBounds

		Column {
			id: columb
			width: win.width

			onHeightChanged: console.log(height)

			ItemDelegate {
				width: parent.width
				text: "Server " + server.address
			}

			ItemDelegate {
				width: parent.width
				text: "Virtual IP"

				TextField {
					anchors.right: parent.right
					anchors.verticalCenter: parent.verticalCenter
					onTextChanged: server.ipT = text
					Component.onCompleted: text = server.ipT
				}
			}

			ItemDelegate {
				width: parent.width
				text: "External IP"

				TextField {
					anchors.right: parent.right
					anchors.verticalCenter: parent.verticalCenter
					onTextChanged: server.externIP = text
					Component.onCompleted: text = server.externIP
				}
			}

			ItemDelegate {
				width: parent.width
				text: "DR"

				Row {
					anchors.right: parent.right
					anchors.verticalCenter: parent.verticalCenter

					Label {
						text: server.drIP + ":" + server.drPort
						anchors.verticalCenter: parent.verticalCenter
					}

					ToolSeparator {
					}

					TextField {
						onTextChanged: server.drotherIP = text
						Component.onCompleted: text = server.drotherIP
					}
				}
			}

			ItemDelegate {
			}

			Neightbor {
				neightbor: server.neighbor1
			}

			Neightbor {
				neightbor: server.neighbor2
			}

			Neightbor {
				neightbor: server.neighbor3
			}

			Neightbor {
				neightbor: server.neighbor4
			}
		}
	}

	Log {
		id: log;
	}
}
